# VideoGrabber #

A simple selenium based application for downloading videos from the ACCA website

## How to configure ##

The selenium driver path is configured statically in the build.sbt file and the download folder can be configured from the application.conf file.