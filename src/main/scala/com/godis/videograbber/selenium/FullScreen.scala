package com.godis.videograbber.selenium

import org.openqa.selenium.WebDriver

/**
 * Created by Abim on 15/01/2016.
 */
trait FullScreen {

  val webDriver: WebDriver

  webDriver.manage().window().maximize()
}
