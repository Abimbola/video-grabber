package com.godis.videograbber.pages

import com.godis.videograbber.selenium.FullScreen
import org.openqa.selenium.WebDriver
import org.scalatest.selenium.WebBrowser

/**
 * Created by Abim on 15/01/2016.
 */
case class ModulePage(address: String)(implicit val webDriver: WebDriver) extends WebBrowser with FullScreen {

  def open() = { go to address; this }

  lazy val lessons = findAll(cssSelector(".content .entry-content h4 a"))
    .map(a => Lesson(a.text, a.attribute("href").get))
    .toList
    .sortBy(_.title)

  def lessonPage(lesson: Lesson) = LessonPage(lesson).open()
}

case class Lesson(title: String, url: String)