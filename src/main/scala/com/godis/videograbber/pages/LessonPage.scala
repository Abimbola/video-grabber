package com.godis.videograbber.pages
import java.io.File
import java.net.URL
import java.nio.file.{Files, Paths}

import org.openqa.selenium.WebDriver
import org.scalatest.selenium.WebBrowser

import scala.sys.process._

/**
 * Created by Abim on 15/01/2016.
 */
case class LessonPage(lesson: Lesson)(implicit val webDriver: WebDriver) extends WebBrowser {

  println(lesson)

  def open() = { go to lesson.url; this }

  def video() = {
    val videoIFrame = find(cssSelector("iframe[src*=\"//player.vimeo\"]")).get
    switchTo(frame(videoIFrame))

    find(cssSelector("#player video"))
      .flatMap(_.attribute("src"))
      .map(Video(lesson.title, _))
      .get
  }
}

case class Video(name: String, url: String) {

  val filename = name.replaceAll("[^a-zA-Z0-9.-]", " ")

  def download(folder: String): String = {
    val fileLocation = s"$folder/$filename.mp4"

    val filePath = Paths get fileLocation
    val downloaded = Files.exists(filePath) && Files.size(filePath) > 10 * 1000 * 1000

    if (!downloaded) {
      println(s"downloading $name...")

      Files.createDirectories(filePath.getParent)

      new URL(url) #> new File(fileLocation) !!

      s"$name downloaded..."
    } else {
      s"$name skipped..."
    }
  }
}
