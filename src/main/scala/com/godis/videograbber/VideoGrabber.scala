package com.godis.videograbber

import com.godis.videograbber.pages.{LessonPage, ModulePage}
import com.typesafe.config.ConfigFactory
import org.openqa.selenium.chrome.ChromeDriver
import org.scalatest._

import scala.language.postfixOps

/**
  * Created by abim on 15/01/2016.
 */
object VideoGrabber extends FlatSpec with GivenWhenThen with App {

  Given("I open a browser")
  implicit val webDriver = new ChromeDriver()

  When("I navigate to the ACCA F6 Downloads Page")
  val F6Page = ModulePage("http://opentuition.com/acca/f6/acca-f6-lectures/")
  F6Page.open()

  And("I open the lecture pages")
  val lessonPages = F6Page
    .lessons
    .map(LessonPage(_))
  require(lessonPages.size == 65)

  Then("I download the videos")
  val videoFolderPath = ConfigFactory.load().getString("videos.path")
  lessonPages
    .foreach(page =>
      println(
        page
          .open()
          .video()
          .download(folder = videoFolderPath)))

  Then("I close the browser")
  webDriver.quit()
}
