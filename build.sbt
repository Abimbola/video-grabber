name := "VideoGrabber"

version := "1.0"

scalaVersion := "2.11.7"

parallelExecution in Compile := false

fork in Compile := true

javaOptions in Compile <<= (resourceDirectory in Compile) map { resourceDir =>
  import com.typesafe.config.ConfigFactory
  val config = ConfigFactory.parseFile(resourceDir / "application.conf")
  val webDriverPath = config.getString("webdriver.path")
  s"-Dwebdriver.chrome.driver=$webDriverPath" :: Nil
}

libraryDependencies := Seq(
  "com.typesafe" % "config" % "1.3.0",
  "org.scalatest" %% "scalatest" % "2.2.6",
  "org.scalacheck" %% "scalacheck" % "1.12.5",
  "org.seleniumhq.selenium" % "selenium-java" % "2.49.0"
)
